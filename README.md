### Project Name: New York City Restaurant Violations
##### Project Abstract
`New York City Restaurant Violations` is a flutter project showcasing the use of [paging](https://whatis.techtarget.com/definition/pagination) items in a `ListView` using the [ScrollController](https://docs.flutter.io/flutter/widgets/ScrollController-class.html) class. As a sample list of data, I am using data from [DOHMH New York City Restaurant Inspection Results](https://data.cityofnewyork.us/Health/DOHMH-New-York-City-Restaurant-Inspection-Results/43nn-pn8j) available publicly, inspired by a bad breakfast I had in a cafe in Bangladesh, which caused me to have food poisoning.
##### Project Platform
I tested it only for **Android**. This project is void of any platform specific code. So it should work for both **Android** and **iOS**. But whether I have set it up for iOS correctly or not is something I am not sure of.  
`flutter doctor -v` output:

[✓] Flutter (Channel beta, v1.2.1, on Linux, locale en_US.UTF-8)
    • Flutter version 1.2.1 at /home/shabab/flutter
    • Framework revision 8661d8aecd (2 weeks ago), 2019-02-14 19:19:53 -0800
    • Engine revision 3757390fa4
    • Dart version 2.1.2 (build 2.1.2-dev.0.0 0a7dcf17eb)

[✓] Android toolchain - develop for Android devices (Android SDK version 28.0.3)
    • Android SDK at /home/shabab/Android/Sdk
    • Android NDK location not configured (optional; useful for native profiling support)
    • Platform android-28, build-tools 28.0.3
    • ANDROID_HOME = /home/shabab/Android/Sdk
    • Java binary at: /home/shabab/android-studio/jre/bin/java
    • Java version OpenJDK Runtime Environment (build 1.8.0_152-release-1248-b01)
    • All Android licenses accepted.

[✓] Android Studio (version 3.3)
    • Android Studio at /home/shabab/android-studio
    • Flutter plugin version 32.0.1
    • Dart plugin version 182.5124
    • Java version OpenJDK Runtime Environment (build 1.8.0_152-release-1248-b01)

[!] IntelliJ IDEA Ultimate Edition (version 2018.2)
    • IntelliJ at /home/shabab/Idea/idea-IU-182.4892.20
    ✗ Flutter plugin not installed; this adds Flutter specific functionality.
    ✗ Dart plugin not installed; this adds Dart specific functionality.
    • For information about installing plugins, see
      https://flutter.io/intellij-setup/#installing-the-plugins

[!] VS Code (version 1.28.2)
    • VS Code at /usr/share/code
    ✗ Flutter extension not installed; install from
      https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter

[✓] Connected device (1 available)
    • MI 5 • da4eaab • android-arm64 • Android 8.0.0 (API 26)

! Doctor found issues in 2 categories.


##### App Workflow
- First turn on the wifi and then open the app. You will be shown a loading screen like this.

![](init_loading.png)

- Then when loading ends a list of restaurant violations show up in `CardView`.The list item cards show:
     1. Restaurant name for violation along with the location of the restaurant as a subtitle.
     2. Phone number of the restaurant.
     3. Current grade of the restaurant.
     4. The description of the violation for the mentioned restaurant.
     
![](show_list_init.png)

- Once you go to the **bottom** then you see a loading screen again. What happens here is that the app requests for new data and shows once the request is successful. Thus, achieving the pagination effect.

![](show_list_loading.png)
![](show_list_loaded.png)

- You can refresh the list by pulling down as one does in Gmail refreshing

![](show_reload.jpg)

- The app needs network connectivity for it to function properly. In case there is no network it shows a ***sorry screen***. In case of a ***sorry screen*** as shown in the picture, the user needs to clear the app from foreground and background and then restart the app by turning the network connection on. You need to retry by pulling down with internet connectivity.

![](no_network_reload.png)

##### Future Plan
- Add search feature. Use the **BLoC** pattern with the all the advantages of **RxDart** to query from the API.
- Publish the app to play store with the added features.

##### Things I loved while building the app
- Fast development with the hot reload led to finish the app in less than a day.
- Was surprised with how beautiful I can make the app with very little code. UI design has never been my strong suite.
- Dart is pretty easy to learn and makes asynchronous coding pretty easy.

##### Challenges faced while building the app
- Keeping the code within the 5kB limit was pretty difficult. Thanks to this [tweet](https://twitter.com/filiphracek/status/1099058521893593088) from [Filip Hráček](https://filiph.net/) I could refactor the code to be in the limit.
- Imposter Syndrome kicks in when you see the app does so little in terms of functionality in order to keep it in the limit. At first I had a splash screen for the app but had to remove the screen entirely.

#### Contact Information
- **Name:** Shabab Karim
- **Twitter Link:** [@PublicFinal](https://twitter.com/PublicFinal)
- **LinkedIn Link:** [Shabab Karim](https://www.linkedin.com/in/shababkarim/)
- **Contact Address:** Banani Block C, House 47 Rd No. 4, Dhaka 1213, Bangladesh.

MIT License

Copyright (c) 2019 Shabab Karim

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
