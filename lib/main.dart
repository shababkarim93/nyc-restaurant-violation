import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

const APP_NM = "NYC Restaurant Violations";
final appClr = Colors.orangeAccent;
void main() => runApp(Mp());

class Mp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: APP_NM,
        theme: ThemeData(primarySwatch: Colors.orange),
        home: Lst());
  }
}

class Lst extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LsSt();
  }
}

class LsSt extends State<Lst> {
  var _er = false;
  var _ls = List();
  var _ct = ScrollController();
  LsSt() {
    _ct.addListener(() {
      if (_ct.offset >= _ct.position.maxScrollExtent &&
          !_ct.position.outOfRange &&
          !_er &&
          _ls.length != 0) {
        fc(_ls.length);
      }
    });
  }
  @override
  void initState() {
    super.initState();
    fc();
  }

  Future<Null> fc([int o = 0]) async {
    try {
      var rs = await http.get(
          "https://data.cityofnewyork.us/resource/9w7m-hzhe.json?\$\$app_token=QPPT5NhFpGkLMX9c5EgCVl2tF&\$limit=10&\$offset=$o");
      if (rs.statusCode == 200) {
        setState(() {
          _er = false;
          _ls.addAll(json.decode(rs.body).map<V>((m) => V.jsn(m)).toList());
        });
      } else {
        setState(() => _er = true);
      }
    } catch (e) {
      setState(() => _er = true);
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
        AppBar(title: Text(APP_NM, style: TextStyle(color: Colors.white))),
        body: RefreshIndicator(
            child: _er
                ? SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Container(
                    height: MediaQuery.of(context).size.height - 80,
                    child: Text("No Network :( Pull down to refresh",
                        style: TextStyle(
                            color: Colors.black, fontSize: 25.0))))
                : ListView.builder(
                itemBuilder: (ct, i) {
                  if (_ls.length == i) {
                    return Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: LinearProgressIndicator());
                  }
                  return LstR(_ls[i]);
                },
                controller: _ct,
                itemCount: _ls.length + 1),
            onRefresh: () => fc(_ls.length - (_er ? 0 : 10))));
  }
}

class LstR extends StatelessWidget {
  final V vln;
  LstR(this.vln);
  Icon ic(ic) => Icon(Icons.location_city, color: Colors.blue[500]);
  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(children: [
          ListTile(
              title: Text(vln.db ?? "No name available"),
              subtitle: Text(vln.lc),
              leading: ic(Icons.location_city)),
          ListTile(title: Text(vln.ph ?? "No phone available"), leading: ic(Icons.contact_phone)),
          ListTile(
            title: Text(vln.gr ?? "No grade available"),
            leading: Icon(Icons.grade, color: Colors.blue[500]),
          ),
          ListTile(
            title: Text(vln.ds ?? "Violation Description not available"),
            leading: Icon(Icons.description, color: Colors.blue[500]),
          )
        ]));
  }
}

class V {
  final lc;
  final ph;
  final db;
  final gr;
  final ds;
  V({this.lc, this.ph, this.db, this.gr, this.ds});
  factory V.jsn(Map<String, dynamic> j) {
    return V(
        lc: "${j['boro']}-${j['street']}-${j['building']}-${j['zipcode']}",
        ph: j['phone'],
        db: j['dba'],
        gr: j['grade'],
        ds: j['violation_description']);
  }
}